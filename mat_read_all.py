def read_mat(infile):
    """read Matlab input
    only vers5
    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    import h5py
    try:
        m = loadmat(infile)  # version 5
    except NotImplementedError:
        f = h5py.File(infile)  # version 7
        m = dict(f)


    print(m)
    return m

read_mat('mat_v73.mat')
read_mat('mat_v5')
